$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 2000 // esto es en milisegundos
    });

    $('#contacto').on('show.bs.modal', function (e) {
        console.log('El modal se esta mostrando');
        $('#contactoBtn').removeClass('btn-outline-primary');
        $('#contactoBtn').addClass('btn-outline-success');
        $('#contactoBtn').prop('disabled', true);
    });

    $('#contacto').on('hidden.bs.modal', function (e) {
        console.log('Se oculto el Boton');
        $('#contactoBtn').removeClass('btn-outline-success');
        $('#contactoBtn').addClass('btn-outline-primary');
        $('#contactoBtn').prop('disabled', false);
    })
});